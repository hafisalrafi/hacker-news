# Hacker News Clone

Clone of hacker news app using ReactJS

### Configure

Get the repo

```sh
git clone https://gitlab.com/hafisalrafi/hacker-news.git
cd hackernews
npm install
npm start
```

### Authors

Hafis Alrafi
